# module-template #

Provides a minimal template for npm module creation using browserify and babelify(for es6 style code).

### How do I get set up? ###

* `npm install`
* run `npm test` to compile your source file(defaults to `index.js`) which will appear in the `public/js` folder